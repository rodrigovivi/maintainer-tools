This documentation covers the tools and workflows for maintaining and
contributing to the Linux kernel DRM subsystem's drm-misc and drm-intel
repositories. The intended audience is primarily the maintainers and committers
of said repositories, but the workflow documentation may be useful for anyone
interested in the kernel graphics subsystem development.

Both drm-misc and drm-intel are maintained using the same tools and very similar
workflows. Both feed to the same testing and integration tree, the drm-tip. The
documentation here is mostly shared, highlighting the differences in workflows
where applicable.

Please use the `dim-tools@lists.freedesktop.org`_ mailing list for
contributions, bug reports, and discussion about the tooling and documentation.

.. _dim-tools@lists.freedesktop.org: https://lists.freedesktop.org/mailman/listinfo/dim-tools

Visit the  `DRM Maintainer Tools
Documentation <https://drm.pages.freedesktop.org/maintainer-tools/>`_ for more
information on these tools.
